import * as vscode from 'vscode';
import { ConfigurapiTreeProvider } from './ConfigurapiTreeProvider';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext)
{
    const rootPath = vscode.workspace.rootPath || '';

    const treeProvider = new ConfigurapiTreeProvider(rootPath);

    vscode.window.registerTreeDataProvider('configurapi', treeProvider);
}

// this method is called when your extension is deactivated
export function deactivate() {}
