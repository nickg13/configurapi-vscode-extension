import * as vscode from 'vscode';
import { join as pathJoin } from 'path';
import * as fs from 'fs';
import * as yaml from 'js-yaml';
import { OpenFile } from './commands/openFile';

export class ConfigurapiTreeProvider implements vscode.TreeDataProvider<ITreeItem>
{
    private _onDidChangeTreeData = new vscode.EventEmitter<ITreeItem | undefined>()
    readonly onDidChangeTreeData: vscode.Event<void | ITreeItem | null | undefined> | undefined = this._onDidChangeTreeData.event

    endpoints: Endpoint[]

    constructor (private workspaceRoot: string)
    {
        const text = fs.readFileSync(pathJoin(this.workspaceRoot, 'src', 'config.yaml'), 'utf-8');
        const data = yaml.load(text) as any;
        this.endpoints = this.getEvents(data);
    }

    refresh(root: ITreeItem): void {
        this._onDidChangeTreeData.fire(root);
    }

    async getHandlerPath(handlerName:string): Promise<string>
    {
        let path = pathJoin(this.workspaceRoot, 'src', 'handlers', `${handlerName}.ts`);
        await fs.promises.access(path);
        return path;
    }

    getTreeItem(element: ITreeItem): ITreeItem | Thenable<ITreeItem>
    {
        if (element instanceof Handler)
        {
            return this.getHandlerPath(element.label as string)
                .then(path => {
                    element.resourceUri = vscode.Uri.file(path);
                    element.command = new OpenFile(element.resourceUri);
                    return element;
                })
                .catch(() => element);
        }
        return element;
    }

    getChildren(element: ITreeItem): vscode.ProviderResult<ITreeItem[]>
    {
        if (!element)
        {
            return this.endpoints
        }
        return element?.children;
    }

    private getEvents(data: { import: string[], api: { events: { name: string, policies: (string|Record<string,any>)[] }[] }}): Endpoint[]
    {
        return data.api.events.map(event => {
            const policies = event.policies.map(policy => new Handler(typeof policy === 'string' ? policy : Object.keys(policy)[0]));
            return new Endpoint(event.name, policies);
        });
    }
}

interface ITreeItem extends vscode.TreeItem
{
    children?: ITreeItem[];
}

class Endpoint extends vscode.TreeItem implements ITreeItem
{
    children: Handler[];

    constructor(label: string, policies: Handler[] = [])
    {
        super(label, vscode.TreeItemCollapsibleState.Collapsed);
        this.children = policies;
    }
}

class Handler extends vscode.TreeItem implements ITreeItem
{
    constructor(label: string)
    {
        super(label);
    }
}
