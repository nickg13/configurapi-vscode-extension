# Change Log

All notable changes to the "configurapi" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

## [0.1.0] 2021-06-04
- Initial release
### Added
- Configurapi tree view - lists events and their policies, which opens the corresponding handler when clicked
