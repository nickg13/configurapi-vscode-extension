# configurapi README

This extension assists with easy viewing of api endpoints and the handlers used by those endpoints

## Features

Creates a configurapi tree in the exporer view that lists api events. Events contain their policies, which opens the corresponding handler file when clicked.

## Requirements

Works with typescript api projects using [configurapi](https://www.npmjs.com/package/configurapi)

## Release Notes

Users appreciate release notes as you update your extension.

### 0.1.0

Initial release
